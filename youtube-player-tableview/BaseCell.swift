//
//  BaseCell.swift
//  youtube-player-tableview
//
//  Created by Samat Murzaliev on 29.07.2022.
//

import Foundation
import UIKit
import SnapKit

class BaseCell: UITableViewCell {
    
    var isLoaded = false
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setModel(_ model: Any?, delegate: Any? = nil, indexPath: IndexPath? = nil) {}
    
    func configure() {
        if !isLoaded {
            isLoaded = true
            
            addSubViews()
            setupUI()
            onViewLoaded()
        }
    }
    
    open func onViewLoaded() {
    }
    
    open func addSubViews() {
    }
    
    open func setupUI() {
    }
}
