//
//  SurveyCell.swift
//  youtube-player-tableview
//
//  Created by Samat Murzaliev on 29.07.2022.
//

import UIKit

class SurveyCell: UITableViewCell {
    
    private lazy var surveyInnerCircle: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "survey.inner.circle")
        view.tintColor = UIColor(named: "mainColor")
        view.isHidden = true
        return view
    }()
    
    private lazy var surveyOuterCircle: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "survey.outer.circle")
        view.tintColor = UIColor(named: "inactive.survey.circle")
        return view
    }()
    
    private lazy var surveyTitle: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .regular)
        view.textColor = .black
        return view
    }()

    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        addSubview(surveyOuterCircle)
        surveyOuterCircle.snp.makeConstraints { make in
            make.height.width.equalTo(26)
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(4)
        }
        surveyOuterCircle.addSubview(surveyInnerCircle)
        surveyInnerCircle.snp.makeConstraints { make in
            make.height.width.equalTo(16)
            make.center.equalToSuperview()
        }
        
        addSubview(surveyTitle)
        surveyTitle.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(4)
            make.left.equalTo(surveyOuterCircle.snp.left).offset(10)
        }
    }
    
    func fill(check: Bool, item: String) {
        surveyTitle.text = item
        if check {
            surveyOuterCircle.tintColor = UIColor(named: "mainColor")
            surveyInnerCircle.tintColor = UIColor(named: "mainColor")
            surveyInnerCircle.isHidden = false
        } else {
            surveyOuterCircle.tintColor = UIColor(named: "inactive.survey.circle")
            surveyInnerCircle.tintColor = UIColor(named: "inactive.survey.circle")
            surveyInnerCircle.isHidden = true
        }
    }
}
