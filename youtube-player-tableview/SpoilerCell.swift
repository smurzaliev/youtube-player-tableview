//
//  SpoilerCell.swift
//  youtube-player-tableview
//
//  Created by Samat Murzaliev on 29.07.2022.
//

import UIKit
import SnapKit

protocol CustomCellUpdater: AnyObject {
    func updateTableView(index: IndexPath)
}

class SpoilerCell: UITableViewCell {
    
    weak var delegate: CustomCellUpdater?
    
    var indexPath: IndexPath?
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var leftVerticalBar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "mainColor")
        return view
    }()
    
    private lazy var spoilerLabel: UILabel = {
        let view = UILabel()
        view.text = "Спойлер"
        view.font = .systemFont(ofSize: 20, weight: .medium)
        view.textColor = .black
        return view
    }()
    
    private lazy var showLabel: UILabel = {
        let view = UILabel()
        view.text = "Показать"
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = UIColor(named: "mainColor")
        return view
    }()
    
    private lazy var expanded = false
    
    var showButton: UIButton = {
        let view = UIButton(type: .system)
        var config = UIButton.Configuration.borderless()
        config.attributedTitle = AttributedString("Показать  ")
        config.attributedTitle?.font = .systemFont(ofSize: 14, weight: .bold)
        config.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        config.image = UIImage(named: "chevron.down")
        config.imagePlacement = .trailing
        config.image?.withTintColor(UIColor(named: "mainColor")!)
        view.tintColor = UIColor(named: "mainColor")
        view.configuration = config
        view.addTarget(nil, action: #selector(surveyExpand(view:)), for: .touchUpInside)
        
        return view
    }()
    
    private lazy var spoilerTableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.estimatedRowHeight = 200
        view.register(VideoCell.self, forCellReuseIdentifier: "VideoCell")

        return view
    }()
    
    var contentBack = UIView()
    
    private var surveyItems: [String] = ["Заведующая отделом образования, науки, культуры, спорта и молодежи администрации президента Галина Байтерек оплатила штрафы за нарушение ПДД. Информацию об этом редакции Kaktus.media подтвердили в пресс-службе патрульной милиции Бишкека."]
    
    private func setupSubview() {
        backgroundColor = UIColor(named: "greyColor")
        contentView.addSubview(contentBack)
        contentBack.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        contentBack.addSubview(backView)
        backView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.top.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-16)
        }
        
        backView.addSubview(leftVerticalBar)
        leftVerticalBar.snp.makeConstraints { make in
            make.width.equalTo(4)
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        backView.addSubview(spoilerLabel)
        spoilerLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.left.equalTo(leftVerticalBar.snp.right).offset(16)
        }
        backView.addSubview(showButton)
        showButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-16)
            make.left.equalTo(leftVerticalBar.snp.right).offset(16)
        }
        
        backView.addSubview(spoilerTableView)
        spoilerTableView.snp.makeConstraints { make in
            make.height.equalTo(0)
            make.top.equalTo(spoilerLabel.snp.bottom).offset(4)
            make.bottom.equalTo(showButton.snp.top).offset(-4)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
        }
    }

    @objc func surveyExpand(view: UIButton) {
        expanded = !expanded

        if expanded {
            var config = UIButton.Configuration.borderless()
            config.attributedTitle = AttributedString("Показать  ")
            config.attributedTitle?.font = .systemFont(ofSize: 14, weight: .bold)
            config.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
            config.image = UIImage(named: "chevron.down")
            config.imagePlacement = .trailing
            config.image?.withTintColor(UIColor(named: "mainColor")!)
            showButton.tintColor = UIColor(named: "mainColor")
            showButton.configuration = config
            
            backView.snp.remakeConstraints { make in
                make.left.equalToSuperview().offset(16)
                make.top.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(-16)
            }
            
            spoilerTableView.snp.remakeConstraints { make in
                make.height.equalTo(0)
                make.top.equalTo(spoilerLabel.snp.bottom).offset(4)
                make.bottom.equalTo(showButton.snp.top).offset(-4)
                make.left.equalToSuperview().offset(16)
                make.right.equalToSuperview().offset(-16)
            }
        } else {
            var config = UIButton.Configuration.borderless()
            config.attributedTitle = AttributedString("Скрыть  ")
            config.attributedTitle?.font = .systemFont(ofSize: 14, weight: .bold)
            config.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
            config.image = UIImage(named: "chevron.up")
            config.imagePlacement = .trailing
            config.image?.withTintColor(UIColor(named: "mainColor")!)
            showButton.tintColor = UIColor(named: "mainColor")
            showButton.configuration = config
            
            backView.snp.removeConstraints()
            
            backView.snp.makeConstraints { make in
                make.height.equalTo(backView.frame.height + spoilerTableView.contentSize.height)
                make.left.equalToSuperview().offset(16)
                make.top.right.equalToSuperview()
                make.bottom.equalToSuperview().offset(-16)
            }
            
            spoilerTableView.snp.removeConstraints()
            spoilerTableView.snp.makeConstraints { make in
                make.height.equalTo(spoilerTableView.contentSize.height)
                make.top.equalTo(spoilerLabel.snp.bottom).offset(4)
                make.bottom.equalTo(showButton.snp.top).offset(-4)
                make.left.equalToSuperview().offset(16)
                make.right.equalToSuperview().offset(-16)
            }
        }
        delegate?.updateTableView(index: indexPath!)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SpoilerCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
        cell.fill(id: "BHACKCNDMW8")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
