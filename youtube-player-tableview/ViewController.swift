//
//  ViewController.swift
//  youtube-player-tableview
//
//  Created by Samat Murzaliev on 29.07.2022.
//

import UIKit
import SnapKit

class ViewController: UIViewController, CustomCellUpdater {
    
    private lazy var videoTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        view.register(VideoCell.self, forCellReuseIdentifier: "VideoCell")
        view.register(SpoilerCell.self, forCellReuseIdentifier: "SpoilerCell")
        view.rowHeight = UITableView.automaticDimension
        view.showsHorizontalScrollIndicator = false
        view.allowsSelection = false
        
        return view
    }()
    
    var spoileCellHeight = 100
    
    private let videIds = ["BHACKCNDMW8", "nwM7uXRkxKs", "JkaxUblCGz0", "NcRifDitRnU","BHACKCNDMW8", "nwM7uXRkxKs", "JkaxUblCGz0", "NcRifDitRnU"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupSubview()
    }
    
    private func setupSubview() {
        view.backgroundColor = .white
        view.addSubview(videoTable)
        videoTable.backgroundColor = UIColor(named: "greyColor")
        videoTable.snp.makeConstraints { make in
            make.top.bottom.right.left.equalToSuperview()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videIds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        if ((index % 2) == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoCell
            cell.fill(id: videIds[indexPath.row])
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpoilerCell", for: indexPath) as! SpoilerCell
            cell.delegate = self
            cell.indexPath = indexPath
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func updateTableView(index: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            self?.videoTable.reloadRows(at: [index], with: .fade)

        }
    }
}
