//
//  VideoCell.swift
//  youtube-player-tableview
//
//  Created by Samat Murzaliev on 29.07.2022.
//

import UIKit
import SnapKit
import youtube_ios_player_helper

class VideoCell: UITableViewCell {
    
    private let player = YTPlayerView()
    private lazy var contentPlayer = UIView()
    
    override func layoutSubviews() {
        setupSubview()
    }
    
    private func setupSubview() {
        
        contentView.addSubview(contentPlayer)
        contentPlayer.snp.makeConstraints { make in
            make.left.right.top.bottom.equalToSuperview()
            make.height.equalTo(240)
        }
        
        contentPlayer.addSubview(player)
        player.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    func fill(id: String) {
        player.load(withVideoId: id)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSubview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
